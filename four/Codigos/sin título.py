#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  sin título.py
#  
#  Copyright 2014 Jose <jose@ubuntu>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import scipy
from scipy import interpolate
from scipy import misc
import Image
import numpy as np

def diferencias_divididas(x, lx, ly):
	ly=ly.tolist()
	print len(lx)
	print len(ly)
	y = 0
	for i in range(len(lx)-1):
			y = (ly[i+1] - ly[i]) / (lx[i+1]-lx[i]) * (x - lx[i]) + ly[i]
	return y
    
def interpolation_matrix_dd(matriz):
	nfilas,ncolumnas=matriz.shape
	matriz_interpolada_filas=np.zeros(shape=(nfilas,ncolumnas*5))
	matriz_interpolada_columnas=np.zeros(shape=(nfilas*5,ncolumnas*5))
	#interpolacion por filas
	for i in range(0,nfilas):
		fila_actual=matriz[i,:]
		for j in range(ncolumnas,ncolumnas*5):
			lx=range(0,j)
			fila_actual=np.append(fila_actual,(diferencias_divididas(j+1,lx,fila_actual)))
		matriz_interpolada_filas[i,:]=fila_actual	
	#interpolacion por columnas	
	
	for i in range(0,ncolumnas):
		columna_actual=matriz[:,i]
		for j in range(nfilas,nfilas*5):
			lx=range(0,j)
			columna_actual=np.append(columna_actual,(diferencias_divididas(j+1,lx,columna_actual)))
		matriz_interpolada_columnas[:,i]=columna_actual
	return matriz_interpolada_columnas		
def open_img_as_rgb():
	im = Image.open("Dataset/homero.png")
	print im.getpixel((1,1))
	r, g, b = im.split()
	return np.array(r),np.array(g),np.array(b)
	'''
	im.show()
	x=im.size[0]
	print x
	y=im.size[1]
	print y
	
	r1 = r.resize((x*5, y*5), Image.BICUBIC)
	g1 = g.resize((x*5, y*5), Image.BICUBIC)
	b1 = b.resize((x*5, y*5), Image.BICUBIC)
	
	r2 = r.resize((x*5, y*5), Image.BILINEAR)
	g2 = g.resize((x*5, y*5), Image.BILINEAR)
	b2 = b.resize((x*5, y*5), Image.BILINEAR)
	
	r3 = r.resize((x*5, y*5), Image.NEAREST)
	g3 = g.resize((x*5, y*5), Image.NEAREST)
	b3 = b.resize((x*5, y*5), Image.NEAREST)
	im1 = Image.merge("RGB", (r1, g1, b1))
	im1.save("BICUBIC.png")
	
	im2 = Image.merge("RGB", (r2, g2, b2))
	im2.save("BILINEAR.png")
	
	im3 = Image.merge("RGB", (r3, g3, b3))
	im3.save("NEAREST.png")
	'''
def main():
	r,g,b=open_img_as_rgb()
	r=interpolation_matrix_dd(r)
	Image.fromarray(r).show()
	return 0

if __name__ == '__main__':
	main()

