from numpy.polynomial import chebyshev
import numpy as np
from scipy.interpolate import barycentric_interpolate
import matplotlib.pyplot as plt
from math import log10, pi, degrees, atan

def f(x, alpha):
    return 1.0 / (alpha + x ** 2)

def two_a(range_n):
	for n in range_n:
		coeffs_cheb = [0] * n + [1]
		T11 = chebyshev.Chebyshev(coeffs_cheb, [-1, 1])

		xp_ch = T11.roots()
		fp_ch = f(xp_ch, 1)
		x_ch = np.linspace(xp_ch[0],xp_ch[-1], num=1000)
		
		x = np.linspace(-1, 1, num=1000)
		y_ch = barycentric_interpolate(xp_ch, fp_ch, x_ch)

		fig, (ax2) = plt.subplots(1, figsize=(8, 4))

		l2, = ax2.plot(x_ch, y_ch)
		ax2.plot(x, f(x, 1), '--', c=l2.get_color())
		ax2.plot(xp_ch, fp_ch, 'o', c=l2.get_color())
		ax2.set_xlim(-1.05,1.05)
		ax2.set_ylim(0.4,1.1)
		ax2.set_title("%d puntos de Chebyshev" % n)

		leg2 = ax2.legend(['Interpolacion', 'Real'])
		leg2.get_frame().set_facecolor('#fafafa')

		plt.show();

def two_b():
	# cantidad de nodos (pares e impares)
	x_to_plot = []
	# Error con respecto a la cantidad de nodos (pares e impares)
	y_to_plot = []

	# cantidad de nodos pares 
	x_pares = []
	# Error con respecto a la cantidad de nodos pares 
	y_pares = []

	# cantidad de nodos impares
	x_impares = []
	# Error con respecto a la cantidad de nodos impares
	y_impares = []

	# pendientes con respecto a los nodos pares
	s_pares = []
	# pendientes con respecto a los nodos impares
	s_impares = []
	# pendiente con respecto a todos los nodos (pares e impares)
	s = []

	for n in range(3,8):
		# crea 50 valores entre -1 y 1
		x = np.linspace(-1, 1, num=1000)

		coeffs_cheb = [0] * n + [1]
		T11 = chebyshev.Chebyshev(coeffs_cheb, [-1, 1])
		# raices de Chebyshev
		xp_ch = T11.roots()
		# puntos de Chebyshev evaluados en la funcion
		fp_ch = f(xp_ch, 1)
		x_ch = np.linspace(xp_ch[0],xp_ch[-1], num=1000)

		# interpolacion utilizando las raices de Chebyshev
		y_ch = barycentric_interpolate(xp_ch, fp_ch, x)
		
		# arreglo con todas las diferencias entre lo real y lo interpolado
		diff = []

		# itera entre los valores reales y los interpolados
		for i in range(len(y_ch)):
			for j in range(len(f(x, 1))):
				if i == j:
					# agrega las diferencias entre lo real y lo interpolado al vector
					diff.append( abs(y_ch[i] - f(x, 1)[j]) )
		diferencia_max = log10(max(diff))
		# estos condicionales son para saber si el punto de Chebyshev es par o impar
		if n%2 == 1:
			if len(x_impares) > 0:
				# agrega la pendiente al vector de pendientes impares
				s_impares.append((diferencia_max-y_impares[-1])/float(n - x_impares[-1]))
			x_impares.append(n)
			y_impares.append(diferencia_max)
			
		elif n%2 == 0:
			if len(x_pares) > 0:
				s_pares.append((diferencia_max-y_pares[-1])/float(n - x_pares[-1]))
			x_pares.append(n)
			y_pares.append(diferencia_max)

		if len(x_to_plot) > 0:
			# agrega la pendiente al vector que tiene las pentiente entre todos los puntos
			s.append((diferencia_max-y_to_plot[-1])/float(n - x_to_plot[-1]))

		x_to_plot.append(n)
		y_to_plot.append(diferencia_max)
	
	fig, (error) = plt.subplots(1, figsize=(10, 4))

	# aumenta el grosor de las lineas
	plt.rc('lines', linewidth = 2)
	recta, = error.plot(x_to_plot, y_to_plot, '-', label="recta error")
	
	error.plot(x_to_plot, y_to_plot, 'o', c=recta.get_color())
	plt.rcdefaults() # devuelve la configuracion por default

	pares, = error.plot(x_pares, y_pares,'--', label="recta nodos pares")
	impares, = error.plot(x_impares, y_impares, '--', label="recta nodos impares")


	plt.legend() # muestra los labels de cada recta
	
	# muestra las pendientes en los puntos especificos
	error.text(5, -1.55, '$s_7:\;%.4f$'%s_pares[0], fontsize = 13, rotation=degrees(atan(s_pares[0])), horizontalalignment='center', verticalalignment='center')
	error.text(4, -1.63, '$s_5:\;%.4f$'%s_impares[0], fontsize = 13, rotation=degrees(atan(s_impares[0])), horizontalalignment='center', verticalalignment='center')
	error.text(3.5, -1.13, '$s_1:\;%.4f$'%s[0], fontsize = 13, rotation=degrees(atan(s[0])) , horizontalalignment='center', verticalalignment='center')
	error.text(4.7, -1.645, '$s_2:\;%.4f$'%s[1], fontsize = 13, rotation=degrees(atan(s[1])), horizontalalignment='center', verticalalignment='center')
	error.text(5.35, -1.89, '$s_3:\;%.4f$'%s[2], fontsize = 13, rotation=degrees(atan(s[2])), horizontalalignment='center', verticalalignment='center')
	error.text(6, -2.38, '$s_6:\;%.4f$'%s_impares[1], fontsize = 13, rotation=degrees(atan(s_impares[1])), horizontalalignment='center', verticalalignment='center')
	error.text(6.5, -2.28, '$s_4:\;%.4f$'%s[3], fontsize = 13, rotation=degrees(atan(s[3])), horizontalalignment='center', verticalalignment='center')
	
	error.set_ylim(-2.8,-1.0)
	error.set_xlim(2.9, 7.1)
	error.set_title("Error en funcion de la cantidad de puntos de Chebyshev")
	plt.show()

def two_c():
	#verdadera pendiente
	sp=[]
	alpha=np.linspace(10.0**(-3), 10**3, num=10)
	for k in alpha:
			# cantidad de nodos (pares e impares)
		x_to_plot = []
		# Error con respecto a la cantidad de nodos (pares e impares)
		y_to_plot = []

		# cantidad de nodos pares 
		x_pares = []
		# Error con respecto a la cantidad de nodos pares 
		y_pares = []

		# cantidad de nodos impares
		x_impares = []
		# Error con respecto a la cantidad de nodos impares
		y_impares = []

		# pendientes con respecto a los nodos pares
		s_pares = []
		# pendientes con respecto a los nodos impares
		s_impares = []
		# pendiente con respecto a todos los nodos (pares e impares)
		s = []
		for n in range(3,8):
			# crea 50 valores entre -1 y 1
			x = np.linspace(-1, 1, num=1000)

			coeffs_cheb = [0] * n + [1]
			T11 = chebyshev.Chebyshev(coeffs_cheb, [-1, 1])
			# raices de Chebyshev
			xp_ch = T11.roots()
			# puntos de Chebyshev evaluados en la funcion
			fp_ch = f(xp_ch, k)
			x_ch = np.linspace(xp_ch[0],xp_ch[-1], num=1000)

			# interpolacion utilizando las raices de Chebyshev
			y_ch = barycentric_interpolate(xp_ch, fp_ch, x)
			
			# arreglo con todas las diferencias entre lo real y lo interpolado
			diff = []

			# itera entre los valores reales y los interpolados
			for i in range(len(y_ch)):
				for j in range(len(f(x, k))):
					if i == j:
						# agrega las diferencias entre lo real y lo interpolado al vector
						diff.append( abs(y_ch[i] - f(x, k)[j]) )
			diferencia_max = log10(max(diff))
			# estos condicionales son para saber si el punto de Chebyshev es par o impar
			if n%2 == 1:
				if len(x_impares) > 0:
					# agrega la pendiente al vector de pendientes impares
					s_impares.append((diferencia_max-y_impares[-1])/float(n - x_impares[-1]))
				x_impares.append(n)
				y_impares.append(diferencia_max)
				
			elif n%2 == 0:
				if len(x_pares) > 0:
					s_pares.append((diferencia_max-y_pares[-1])/float(n - x_pares[-1]))
				x_pares.append(n)
				y_pares.append(diferencia_max)

			if len(x_to_plot) > 0:
				# agrega la pendiente al vector que tiene las pentiente entre todos los puntos
				s.append((diferencia_max-y_to_plot[-1])/float(n - x_to_plot[-1]))

			x_to_plot.append(n)
			y_to_plot.append(diferencia_max)
		sp.append((y_to_plot[len(y_to_plot)-1]-y_to_plot[0])/(x_to_plot[len(y_to_plot)-1]-x_to_plot[0]))
	plt.rc('lines', linewidth = 2)
	plt.title("Grafico 2c")
	plt.ylabel("Pendiente s")
	plt.xlabel("Alpha")
	plt.plot(alpha,sp,'k')
	plt.show()
two_c()
def two_d():
	# cantidad de nodos (pares e impares)
	x_to_plot = []
	# Error con respecto a la cantidad de nodos (pares e impares)
	y_to_plot = []
	s = []

	for n in range(3,50):
		# crea 50 valores entre -1 y 1
		x = np.linspace(-1, 1, num=1000)

		coeffs_cheb = [0] * n + [1]
		T11 = chebyshev.Chebyshev(coeffs_cheb, [-1, 1])
		# raices de Chebyshev
		xp_ch = T11.roots()
		# puntos de Chebyshev evaluados en la funcion
		fp_ch = f(xp_ch, 1)
		x_ch = np.linspace(xp_ch[0],xp_ch[-1], num=1000)

		# interpolacion utilizando las raices de Chebyshev
		y_ch = barycentric_interpolate(xp_ch, fp_ch, x)
		
		# arreglo con todas las diferencias entre lo real y lo interpolado
		diff = []

		# itera entre los valores reales y los interpolados
		for i in range(len(y_ch)):
			for j in range(len(f(x, 1))):
				if i == j:
					# agrega las diferencias entre lo real y lo interpolado al vector
					diff.append( abs(y_ch[i] - f(x, 1)[j]) )
		diferencia_max = log10(max(diff))
		# estos condicionales son para saber si el punto de Chebyshev es par o impar

		if len(x_to_plot) > 0:
			# agrega la pendiente al vector que tiene las pentiente entre todos los puntos
			s.append((diferencia_max-y_to_plot[-1])/float(n - x_to_plot[-1]))

		x_to_plot.append(n)
		y_to_plot.append(diferencia_max)
	
	fig, (error) = plt.subplots(1, figsize=(10, 4))

	recta, = error.plot(x_to_plot, y_to_plot, '-', label="recta error")
	error.plot(x_to_plot[0], y_to_plot[0], 'o', c=recta.get_color())
	error.plot(x_to_plot[-1], y_to_plot[-1], 'o', c=recta.get_color())
	plt.legend() # muestra los labels de cada recta
	
	error.set_title("Error en funcion de la cantidad de puntos de Chebyshev")
	plt.show()

two_d()
