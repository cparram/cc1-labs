function [ matriz_interpolada ] = newtdd_filas_columnas( matriz )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    [fila,columna]=size(matriz);
    matriz_interpolada=ones(fila*5,columna*5);
    lx=0:columna-1;
    ly=linspace(0,columna-1,columna*5);
    matriz_filas=ones(fila,columna*5);
    for i=1:fila
        fila_actual=matriz(i,:);
        c=newtdd(lx,fila_actual,columna);
        for j=1:columna*5
            matriz_filas(i,j)=nest(columna-1,c,ly(j),lx);
        end
    end
    lx1=0:fila-1;
    ly1=linspace(0,fila-1,fila*5);
    for i=1:columna*5
        columna_actual=matriz_filas(:,i);
        c=newtdd(lx1,columna_actual,(fila));
        for j=1:fila*5
            matriz_interpolada(j,i)=nest(fila-1,c,ly1(j),lx1);
        end
    end
    
end

