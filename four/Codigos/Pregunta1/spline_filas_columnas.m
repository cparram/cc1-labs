function [ matriz_interpolada ] = spline_filas_columnas( matriz )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    [fila,columna]=size(matriz);
    matriz_interpolada=ones(fila*5,columna*5);
    lx=0:columna-1;
    ly=linspace(0,columna-1,columna*5);
    matriz_filas=ones(fila,columna*5);
    for i=1:fila
        fila_actual=matriz(i,:);
        c= spline(lx,fila_actual);
        for j=1:columna*5
            matriz_filas(i,j)=ppval(c,ly(j));
        end
    end
    lx1=0:fila-1;
    ly1=linspace(0,fila-1,fila*5);
    for i=1:columna*5
        columna_actual=matriz_filas(:,i);
        c=spline(lx1,columna_actual);
        for j=1:fila*5
            matriz_interpolada(j,i)=ppval(c,ly1(j));
        end
    end
    
end

