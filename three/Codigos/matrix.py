# SVD module

from numpy import *
from numpy.linalg import svd, norm, matrix_rank
import matplotlib.pyplot as plt
from math import log10
from scipy import misc
import Image

DIR_MATRICES = "Dataset_updated/matrices/"
DIR_PICTURES = "Dataset_updated/imagenes/"

EPSILON_MACHINE = 2**(-31)

# return just singular values of matrix A
def singular_values_reduced(A, sigma_complete = True):	
	U, sigma, V = svd(A, full_matrices = False)
	if sigma_complete:
		sigma = diag(sigma)
	return U, sigma, V # matrices are not aligned

# return full singular values of matrix A
def full_singular_values(A):
	U, sigma, V = svd(A, full_matrices = True)
	Sigma = zeros(A.shape) # zero matrix with the same dimension as A
	n = min(A.shape) # min dim(x, y)
	Sigma[:n,:n] = diag(sigma) 
	# dot(U, dot(Sigma,V)) == A with some little differences
	return U, Sigma, V

# for read a matrix from data set
def read_matrix(archive_name):
	return loadtxt(DIR_MATRICES + archive_name)

# return the matrix of pictures
def read_pictures_to_array():
	fractal = misc.imread(DIR_PICTURES + 'fractal.png', flatten=True)
	logo_di = asarray(Image.open(DIR_PICTURES + '/logo-di.png').convert('L'))
	paisaje = asarray(Image.open(DIR_PICTURES + '/paisaje.bmp').convert('L'))
	return fractal, logo_di, paisaje

# return an array ap
def logarithmic_scale(array):
	if len(array.shape) > 1:
		return False
	for i in range(len(array)):
		if array[i] <= 0:
			array[i] = EPSILON_MACHINE
		else:
			array[i] = log10(array[i])		
	return True# return the rough picture pth singular value
def rough_picture(sigma_p, u_col_p, v_row_p):
	return dot([u_col_p] * sigma_p, [v_row_p].T)

# for plot an array
def plot_semilogy(array_list, each = True):
	for i in array_list:
		plt.semilogy(i)
		if each:
			plt.show()
		
	if not each:
		plt.show()	

# for plot an array
def plot(array_list, each = True):
	for i in array_list:
		plt.plot(i)
		if each:
			plt.show()
		
	if not each:
		plt.show()	


# return the rough picture pth singular value
def rough_picture(sigma_p, u_col_p, v_row_p):
	return dot(sigma_p * array([u_col_p]).T, array([v_row_p]))

def relative_error_norm_inf(matrix_real_image, matrix_rough_image):
	return float(norm((matrix_real_image - matrix_rough_image), inf)) / float(norm( matrix_real_image, inf))

def create_array_from_list(list_to_array):
	return array(list_to_array)

def show_image(image):
	plt.imshow(image)
	plt.show()

def dot_3 (m1, m2, m3):
	return dot(dot(m1, m2), m3)

def rank(A):
	return matrix_rank(A)