#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matrix

# to resolve II.a
def svd_each_matrix():
	matrices = ['matriz1.txt','matriz2.txt', 'matriz3.txt',
			 'matriz4.txt', 'matriz5.txt' ]
	# show singular values of matrices
	to_plot = []
	for i in range(1,6):
		A = matrix.read_matrix(matrices[i - 1])
		u, sigma, v = matrix.singular_values_reduced(A, sigma_complete = False)
		sigma_complete = matrix.singular_values_reduced(A, sigma_complete = True)[1]
		
		print "sigma", sigma_complete.shape
		print "u",u.shape
		print "v", v.shape

		if matrix.logarithmic_scale(sigma):		
			to_plot.append(sigma)
		else:
			print "FORMATE SIGMA ERRROR"
	# show singular values of pictures
	for i in matrix.read_pictures_to_array():
		u, sigma, v = matrix.singular_values_reduced(i, sigma_complete = False)
		sigma_complete = matrix.singular_values_reduced(i, sigma_complete = True)[1]
		print "sigma", sigma_complete.shape
		print "u", u.shape
		print "v", v.T.shape
		if matrix.logarithmic_scale(sigma):
			to_plot.append(sigma)
		else:
			print "FORMATE SIGMA ERRROR"
	matrix.plot(to_plot, each = True)

def image_quality_and_singular_values():
	###
	matrices = ['matriz1.txt','matriz2.txt', 'matriz3.txt',
			 'matriz4.txt', 'matriz5.txt' ]
	to_plot = []
	for i in range(0,5):
		A = matrix.read_matrix(matrices[i])
		to_plot.append(A)

	for matrix_real_image in matrix.read_pictures_to_array():
		to_plot.append(matrix_real_image)
		
	for matrix_real_image in to_plot:		
		U, Sigma, V = matrix.full_singular_values(matrix_real_image)
		
		u, sigma, v = matrix.singular_values_reduced(matrix_real_image, sigma_complete = False)

		listamult = []
		singular_values_list = []
		image_quality_list = []
		
		for j in range(0, sigma.size):
			print j
			rough_picture = matrix.rough_picture(sigma[j], U[:,j], V[j,:])
			listamult.append(rough_picture)
			suma = rough_picture

			if j > 0:				
				for	t in range(0, j-1):
					suma = suma + listamult[t]
			
			relavite_error = matrix.relative_error_norm_inf(matrix_real_image, suma)
			

			image_quality = 1 / float(relavite_error)

			image_quality_list.append(image_quality)
			singular_values_list.append(sigma[j])

		singular_values = matrix.create_array_from_list(singular_values_list)
		if matrix.logarithmic_scale(singular_values):
			matrix.plot([singular_values, image_quality_list], each = False)
		else:
			print "FORMATE SIGMA ERROR"
			
def compression_ratio_and_image_quality():
	matrices = ['matriz1.txt','matriz2.txt', 'matriz3.txt',
			 'matriz4.txt', 'matriz5.txt' ]
	to_plot = []
	for i in range(0,5):
		A = matrix.read_matrix(matrices[i])
		to_plot.append(A)

	for matrix_real_image in matrix.read_pictures_to_array():
		to_plot.append(matrix_real_image)
		
	for i in to_plot:			
		U, Sigma, V = matrix.full_singular_values(i)
		u, sigma, v = matrix.singular_values_reduced(i, sigma_complete = False)
		
		m, n = i.shape
		memory__original_image = 4 * n * m

		matrix_ph_singular_value = []
		compression_ratio_list = []
		image_quality_list = []

		for p in range(0, sigma.size):
			rough_picture = matrix.rough_picture(sigma[p], U[:,p], V[p,:])
			print p
			matrix_ph_singular_value.append(rough_picture)

			suma = rough_picture

			if p > 0:				
				for	t in range(0, p-1):
					suma = suma + matrix_ph_singular_value[t]
			
			relavite_error = matrix.relative_error_norm_inf(i, suma)
			

			image_quality = 1 / float(relavite_error)
			
			memory_rough_image = 4* m * p + 4 * p + 4 * n * p
			compression_ratio = float(memory_rough_image) / float(memory__original_image)
			
			image_quality_list.append(image_quality)
			
			compression_ratio_list.append(compression_ratio)

		matrix.plot_semilogy([compression_ratio_list, image_quality_list], each = False)


def main():
	#factorization_svd(matrix.create_array_from_list([[0,1,3],[1,3,1],[2,-1,3],[0,1,-1]]))
	#svd_each_matrix()
	image_quality_and_singular_values()
	#compression_ratio_and_image_quality()

# to resolve I 
def factorization_svd(A):
	from numpy import dot
	# reduced svd
	u, sigma, v = matrix.singular_values_reduced(A, sigma_complete = True)
	print A
	print u
	print sigma
	print v.T

	# full svd
	U, Sigma, V = matrix.full_singular_values(A)
	print U
	print Sigma
	print V.T
	#print matrix.dot(U, Sigma, V)
	#matrix.show_image(matrix.dot(U, Sigma, V))

if __name__ == '__main__':
	main()
