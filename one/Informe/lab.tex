\documentclass[10pt]{article}
\usepackage{color}
\usepackage{listings}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}  
\usepackage{float}  
\usepackage{amsmath}
\usepackage[colorlinks=true,linkcolor=blue,urlcolor=blue]{hyperref}
\begin{document}
\begin{center}
\vspace*{0.9 in}
{\huge\bf Laboratorio Computación Científica}\\ \bigskip {\large Aritmética punto flotante\\ \smallskip Significancia\\}
\end{center}
\vspace*{3.5 in}
\begin{flushright}
{\bf Profesor de Cátedra}\\ Ricardo León \\\medskip
{\bf Integrantes}\\ José Pablo del Río 201173592-7 \\ jose.delrio@alumnos.usm.cl\\ \medskip
César Parra 201173051-8 \\ cesar.parra@alumnos.usm.cl
\end{flushright}
\vspace*{0.6 in}
\section{Introducción}
\smallskip
Sabemos que la recta numérica de los enteros es infinita, por lo que en las máquinas actuales (autómatas finitos), el poder representar esta recta se puede tornar un problema muy grande, siendo el rango representable dependiendo de la cantidad de bits utilizados para ello.\\ \\
En este informe veremos cómo actúa la aritmética de número en su representación de punto flotante con doble precisión, verificando esto con la ayuda del lenguaje de programación Python. También se vera cómo afecta la pérdida de significancia en el almacenamiento de números en precisión doble cuya representación en binario normalizado posea más bits que lo almacenable en precisión doble, mostrando la significancia perdida  mediante el error relativo.\\

 
\section{Desarrollo y análisis de resultados}
\subsection{Representación punto flotante}\label{sec:floatingpoint}
El Instituto de Ingenieros Eléctricos y Electrónicos presenta un estándar para aritmética en punto flotante  \textbf{IEEE 754} que es el estándar más extendido, el cual en resumen si un número en su representación binaria tiene la siguiente forma:
\[signo\ast 2^{\:exponente}\ast1.mantisa \]
Entonces, su representación punto flotante será;
\[signo\quad exponente\quad mantisa \]
Este estándar especifica cuatro formatos para la representación de valores en coma flotante: precisión simple (32 bits), precisión doble (64 bits), precisión simple extendida (mayor o igual a 43 bits, no usada normalmente) y precisión doble extendida (mayor o igual 79 bits, usualmente implementada con 80 bits). Por lo que para efectos de este informe se utilizará una precisión doble ya que Python tiene un manejo de sus números con doble precisión.\\\\
El signo tiene un solo bit, siendo 1 para negativo y 0 para positivo. En cuanto al exponente se utiliza un sesgo para evitar almacenar exponentes negativos, siendo para doble precisión un sesgo de 1023, mientras que la mantisa, de 52 bits, almacena el número binario normalizado.\\\\
Notamos, entonces que para doble precisión utilizamos \[1_{\;signo} + 11_{\;exp} + 52_{\;mantisa} = 64\;bits\]
\subsubsection{Expresión 1}\label{sec:expresion1}
 
En cuanto al desarrollo de la aritmética de punto flotante, se calculó la siguiente expresión en python: 
\begin{equation}\label{eq:expresion1}
5 - 2^{-53}
\end{equation}  
Retornando el siguiente resultado:
\lstinputlisting{../Codigos/return_fp-1.txt}
Claramente, es como si al número cinco no se le estuviera restando nada pero ¿qué sucede con el resultado entregado?\footnote{pregunta \textit{\{I\}}.1.(a) del enunciado}.\\Como Python almacena todos los números con doble precisión, al aplicar la aritmética anterior, ocurre lo siguiente.
\begin{enumerate}
 \item[1.]Se obtiene la representación binaria del número $5_{(10)}$, que es $101.0_{(2)}$.
 \item[2.]Luego, se normaliza su representación binaria, quedando: \[101.0\cdot2^0 = 1.01\ldots0\cdot2^2\]
 \item[3.]Por otro lado la expresión $2^{-53}_{(10)} = 2^{-53}_{(2)}$.\footnote{Porque la base es dos}\\
 
 Se hace notar que ambos números por sí solos, sí se pueden representar y almacenar en punto flotante, teniendo la siguiente representación:\\
 
 Para $5_{(10)}$, el bit del signo será 0 ya que es un número positivo. En cuanto al exponente será $2 + 1023 = 1025_{(10)} = 10000000001_{(2)}$, debido al sesgo. Y la mantisa será el número normalizado obtenido anteriormente, es decir:
 \[ 
   \underbrace{0}_\text{signo(+)}
   \underbrace{1\,0\,0\,0\,0\,0\,0\,0\,0\,0\,1}_\text{exponente}\;
   \underbrace{0\,1\,0\,0\,0\,0\,\ldots0\,0\,0}_\text{mantisa (52 bits)}  
 \footnote{Identificamos apropiadamente el signo, exponente y mantisa}\]
 
 Luego para $2^{-53}_{(10)}$, el bit del signo será 0 ya que es un número positivo. En cuanto al exponente será $-53 + 1023 = 970_{(10)} = 01111001010_{(2)}$, debido al sesgo. Y la mantisa será cero, es decir:
 \[ 
   \underbrace{0}_\text{signo(+)}
   \underbrace{0\,1\,1\,1\,1\,0\,0\,1\,0\,1\,0}_\text{exponente}\;
   \underbrace{0\,0\,0\,0\,0\,0\,\ldots0\,0\,0}_\text{mantisa (52 bits)}  
 \footnote{Identificamos apropiadamente el signo, exponente y mantisa}\]
 \newpage
 Entonces ¿qué sucede? ¿por qué al resolver la expresión (\ref{eq:expresion1}) pareciera que el número $2^{-53}$ no se toma en cuenta?. Para aplicar en ambos número anteriores alguna operación, como en este caso una resta, debemos tener en cuenta el valor de sus exponentes, siendo mucho más fácil resolver la expresión cuando ambos números tienen el mismo exponente.   
 \item[4.]Entonces tenemos la siguiente expresión: \[1.01\cdot2^2 - 2^{-53} = 1.01\cdot2^2-2^{-55}\cdot2^2\]
 Es decir,
 \begin{align*}
 1\,.\overbrace{0\,1\,0\ldots0\,0}^\text{52 bits}\,\qquad\cdot\,2^2 \\
 -\;\;\;\underline{0\,.\,0\,0\,0\ldots0\,0\,\textcolor{red}{0\,0\,1}\cdot2^2} \\
 1\,.\,0\,1\,0\ldots0\,0\;\;\;\,\quad\cdot2^2
 \end{align*}
 
 Siendo los bit en rojo los bit de redondeo. Además mencionamos que el segundo número que se resta tiene 55 bits          después de la coma. Por tanto el resultado se obtiene sin opción de redondear, que es: 
\begin{equation} \label{eq:floatpoint}
 \;1\,.\,0\,1\cdot2^2\;=\;5_{(10)}
\end{equation} 
 
\end{enumerate}
Entonces los cuatro puntos anteriores dan cuenta de por qué Python retorna $5.00$ al ejecutar la expresión (\ref{eq:expresion1}).\\\\
Por otro lado tenemos el término $\in_{mach}$\footnote{pregunta \textit{\{I\}.1.(b) del enunciado}}, el cual es la distancia entre 1 y el menor número representable mayor a 1, es decir,
\begin{align*}
 1\,.\overbrace{0\,0\,0\ldots0\,0}^\text{52 bits}\cdot\;2^2 \\
 -\;\;\;\underline{1\,.\,0\,0\,0\ldots0\,1\cdot2^2} \\
 \in_{mach}\;=\;0\,.\,0\,0\,0\ldots0\,1\cdot2^2
\end{align*}
Este termino nos permite redondear números, en cuanto sea necesario, pero en nuestro problema anterior (\ref{eq:expresion1}) no fue necesario redondear, por lo tanto $\in_{mach}$ no influyó en el termino anterior.
\subsubsection{Expresión 2}\label{sec:expresion2}
Al igual que en la sección \ref{sec:expresion1}, se calculó la siguiente expresión:
\begin{equation}\label{eq:expresion2}
(5 + 3\cdot2^{-53}) + 5
\end{equation}
Obteniendo su resultado exacto aritméticamente \footnote{pregunta \textit{\{I\}}.2.(a)}
\begin{equation}\label{eq:expresion2_result_certain}
(5 - 3\cdot2^{-53}) + 5 = 9,999999999999999666933092612453
\end{equation}
Luego, calculando su resultado en python, tenemos lo siguiente \footnote{pregunta extit{\{I\}}.2.(b)}:
\lstinputlisting{../Codigos/return_fp-2.txt}
Otra vez, nos damos cuenta que al obtener el resultado de la expresión computacionalmente, da la impresión de que el número $3\cdot2^{-53}$ es igual a cero, pero ¿es realmente cero?
Bien sabemos que el número cero, tiene dos representaciones, ya que por el bit del signo puede ser +0 ó -0, es decir\footnote{pregunta \textit{\{I\}}.2.(c)}:
\[ 
   +0\;=\;\underbrace{0}_\text{signo}
   \underbrace{0\,0\,0\,0\,0\,0\,0\,0\,0\,0\,0}_\text{exponente}\;
   \underbrace{0\,0\,0\,0\,0\,0\,\ldots0\,0\,0}_\text{mantisa (52 bits)}  
\]
Y
\[ 
   -0\;=\;\underbrace{1}_\text{signo}
   \underbrace{0\,0\,0\,0\,0\,0\,0\,0\,0\,0\,0}_\text{exponente}\;
   \underbrace{0\,0\,0\,0\,0\,0\,\ldots0\,0\,0}_\text{mantisa (52 bits)}  
\]
Notamos además que el cero está desnormalizado.
Luego, el número $3\cdot2^{-53}$ es igual a: \[2\cdot2^{-53} + 2^{-53} = 2^{-52} + 2^{-53} = 2^{-52} + 2^{-1}\cdot2^{-52} \]
Es decir, 
\begin{align*}
 1\,.\overbrace{0\,0\,0\ldots0\,0}^\text{52 bits}\,\cdot\,2^{-52} \\
 +\;\;\;\underline{0\,.\,1\,0\,0\ldots0\,0\cdot2^{-52}} \\
 1\,.\,1\,0\,0\ldots0\,0\cdot2^{-52}
 \end{align*}
Por lo tanto siendo su representación en punto flotante, depende del signo que en este caso sería 0, ya que estamos tratando con un número positivo. En cuanto al exponente tenemos, $-52 + 1023 = 971_{10} = 01111001011_{(2)}$. Y la mantisa serán los 52 bit mostrados en la suma anterior:
\[ 
   \underbrace{0}_\text{signo(+)}
   \underbrace{0\,1\,1\,1\,1\,0\,0\,1\,0\,1\,1}_\text{exponente}\;
   \underbrace{1\,0\,0\,0\,0\,0\,\ldots0\,0\,0}_\text{mantisa (52 bits)}  
\]
Es claro que el número $3\cdot2^{-53}$ es muy distinto al cero en su representación punto flotante. Lo que está pasando es muy parecido a lo que pasó en la sección \ref{sec:expresion1}, ya que tenemos lo siguiente:
\[\lbrace(5 - 3\cdot2^{-53}) + 5\rbrace_{(10)} = \lbrace(1.01\cdot2^2 - 1.1\cdot2^{-52}) + 1.01\cdot2^2\rbrace_{(2)}\]
\[\lbrace(1.01\cdot2^2 - 1.1\cdot2^{-52}) + 1.01\cdot2^2\rbrace_{(2)} = \lbrace(1.01\cdot2^2 + 1.1\cdot2^{-54}\cdot2^2) + 1.01\cdot2^2\rbrace_{(2)} \]
 Es decir,
\begin{align*}
1\,.\overbrace{0\,1\,0\ldots0\,0}^\text{52 bits}\,\qquad\cdot\,2^2 \\
-\;\;\;0\,.\,0\,0\,0\ldots0\,0\,\textcolor{red}{0\,1\,1}\cdot2^2 \\
+\;\;\;\underline{1\,.\,0\,1\,0\ldots0\,0\;\;\;\quad\cdot\,2^2}\\
1\,0\,.\,1\,0\,0\ldots0\,0\;\;\;\,\quad\cdot2^2\\
=\;\;\;1\,.\,0\,1\,0\ldots0\,0\;\;\;\,\quad\cdot2^3
\end{align*}
Así podemos entender por qué Python retorna $10.0$ al ejecutar la expresión (\ref{eq:expresion2}).\\Mencionamos que además que el término $\in_{mach}$ nuevamente no influyó en el resultado aritmético computacional, ya que no fue necesario aplicar un redondeo
\subsection{Pérdida de significancia}
Se  pide encontrar el menor valor de p para que la expresión evaluada en x=$10^{-p}$ en precisión doble no tenga el numero correcto de dígitos significativos, para ello haciendo uso del hint  entregado se calcula el siguiente limite para ver la convergencia de la expresión:
\begin{equation}
\lim\limits_{x\rightarrow 0}\dfrac{e^{x}+cos(x)-sin(x)-2}{x^{3}}
\end{equation}
Se tiene una expresión de la forma $\dfrac{0}{0}$ por lo que haciendo uso de l'hopital (derivando la expresión del denominador y nominador) se tiene:
\begin{equation}
\lim\limits_{x\rightarrow 0}\dfrac{e^{x}-sin(x)-cos(x)}{3x^{2}}
\end{equation}
Nuevamente se tiene una expresión de la forma $\dfrac{0}{0}$ por lo que haciendo uso l'hopital nuevamente se tiene:
\begin{equation}
\lim\limits_{x\rightarrow 0}\dfrac{e^{x}-cos(x)+sin(x)}{6x}
\end{equation}
Nuevamente se tiene una expresión de la forma $\dfrac{0}{0}$ por lo que haciendo uso l'hopital nuevamente se tiene:
\begin{equation}
\lim\limits_{x\rightarrow 0}\dfrac{e^{x}+sin(x)+cos(x)}{6}
\end{equation}
Evaluando se tiene que:
\begin{equation}
\lim\limits_{x\rightarrow 0}\dfrac{e^{x}+sin(x)+cos(x)}{6}=\dfrac{2}{6}=\dfrac{1}{3}
\end{equation}
Cuando x tiende a 0, la expresión convergerá a el valor $\dfrac{1}{3}$, al aumentar el valor de p, lo que realmente se esta obteniendo es un numero cada vez más pequeño y cercano a 0 para x, por lo tanto es lógico decir que este numero sera cada vez más cercano a la convergencia de la expresión,tomando en cuenta esto se evaluara la expresión con x=$10^{-p}$ con p = 1,2,3,.....,20, con el código confeccionado en python ubicado en  el anexo, para observar el comportamiento de los valores y así encontrar el menor p cuyas cifras significativas no sean correctas con respecto a los valores anteriores.\\Se genero la siguiente tabla con p=1,2,3,...,7 debido a que con esa información ya se encontró lo buscado:
\begin{table}[h]
\centering
\begin{tabular}{|l | c | r|}
\hline
p=  & valor\\
\hline
1 & 0.341666706845\\
\hline
2 & 0.334166666782\\
\hline
3 & 0.333416849685\\
\hline
4 & 0.333510996597\\
\hline
5 & 0.44408920985\\
\hline
6 & 0.0\\
\hline
7 & -222044.604925\\
\hline
\end{tabular}
\vspace{0.01 in}\\\textbf{Tabla 1:} Tabla de valores respecto a p.
\end{table}
Se puede observar que en p=5 las cifras significativas con respecto a los valores anteriores no son las correctas, además se puede ver que cambia radicalmente el valor de la expresión evaluada y por lo explicado anteriormente aritméticamente esto no tiene sentido por lo que se esta en presencia de errores de cancelación y perdida de información producto del almacenamiento en precisión doble de python.\\
a) Se tiene que el valor aritmético exacto de la expresión obtenido por la convergencia de la expresión cuando x tiende a 0 (calculado previamente) es
 $\dfrac{1}{3}$, este numero representado en binario de la forma subnormal es $0.\overline{01}$ y normalizando se obtiene $1.\overline{01}*2^{-2}$ cuya representación en precisión doble es:
  \[ 
   1.
   \underbrace{0\,1\,0\,1\,0\,1\,\ldots\,0\,1}_\text{mantisa (52 bits)}
|0\,1....*2^{-2}   
      \] Se esta en presencia de un numero cuya representación en binario es periódica (infinitos bit) y no es almacenable sin la ocurrencia de un truncamiento y redondeo en caso de ser necesario, por lo tanto como en el bit numero 52 es 1 y el numero 53 es 0 no se produce un redondeo, pero si de un truncamiento cuya ocurrencia genera una perdida de: 
$0.\overline{01}*2^{-2}*2^{-53}$ obteniendo que  $\dfrac{1}{3}$ almacenado en precisión doble es: 
\begin{equation}
fl(\dfrac{1}{3})=(\dfrac{1}{3}-\dfrac{1}{3}*2^{-55})
\end{equation}
Por formula tenemos que el error relativo es el siguiente:
\begin{equation}
\dfrac{|(\dfrac{1}{3}-\dfrac{1}{3}*2^{-55})-\dfrac{1}{3}|}{|\dfrac{1}{3}|}=2^{-55}
\end{equation}\\b) La relación que existe entre el error relativo y $\varepsilon_{mach}$, cuando se efectúa un truncamiento, es que siempre se tiene que cumplir la siguiente expresión cuando existe un truncamiento:
\begin{equation}
\dfrac{|fl(1/3)-\dfrac{1}{3}|}{|\dfrac{1}{3}|}\leq\varepsilon_{mach}
\end{equation}\\Esto ocurre por que al truncar un numero en precisión doble, lo que esta sucediendo es que se esta cortando el numero después del bit 52 en la mantisa, en el peor de los casos se podría tener que después del bit 52 hubieran infinitos bit 1 esto significara la peor pérdida de significancia posible, pérdida que redondeada seria exactamente $\varepsilon_{mach}$, y  dado a que el error relativo es el error que existe entre lo almacenado y el valor real, en otras palabras la pérdida, este  siempre es $\leq\varepsilon_{mach}$. Reemplazando los valores en (12)  se obtiene:
\begin{equation}
2^{-55}\leq\varepsilon_{mach}=2^{-52}
\end{equation}\\Con esto se puede apreciar que se cumple para este error relativo.
\section{Conclusión}
En cuanto a la representación punto flotante de los números nos dimos cuenta que hay número pequeños que sí se pueden representar en punto flotante, como por ejemplo $2^{-53}$, siendo distinto de cero. El problema en cuanto a la representación ocurre cuando se le aplica una operación de suma o resta, entre un número muy pequeño y un número mucho mayor, ya que el resultado de la operación implica un desplazamiento en la mantisa del número pequeño debido al exponente, truncándose la mantisa, hasta el punto que para que la operación se resuelva exactamente se necesitan más de los 52 bits que realmente se utilizan en la mantisa, por lo que se trunca el resultado, sumando o restando prácticamente cero. Estos errores en representación se vieron tanto en la sección \ref{sec:expresion1} como en \ref{sec:expresion2}. \\
Podemos decir también que cuando se realiza aritmética computacional con número pequeños, no necesariamente está la presencia de $\in_{mach}$, utilizándose sólo cuando se necesita redondear números, ya que en la sección \ref{sec:floatingpoint}\\También nos dimos cuenta de que para números cuya representación en binario tiene una parte periódica, son imposibles almacenarlos en alguna precisión finita, como la precisión doble, sin que haya una perdida de información y el error relativo entre el valor aritmético y la representación de el mismo numero en precisión doble siempre sera menor y/o igual a $\varepsilon_{mach}$.  
\section{Referencias}
\begin{enumerate}
\item \url{http://es.wikipedia.org/wiki/IEEE_coma_flotante}
\item \url{http://www-2.dc.uba.ar/materias/oc1/2005/documentos/docu_float_conver.html}
\end{enumerate}
\section{Anexo}
\subsection{Codigos}
\begin{enumerate}
\item{Código utilizado en Pregunta 1, para obtener los valores de las expresiones}
\lstinputlisting[language=Python]{../Codigos/p1a.py}

\lstinputlisting[language=Python]{../Codigos/p1b.py}

\item{Código utilizado en Pregunta 2, para obtener los valores de la tabla.}
\lstinputlisting[language=Python]{../Codigos/p2.py}
\end{enumerate}
\end{document}
